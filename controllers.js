const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('./conexion');

const aaaaaaa = (porqeuria) => {
    console.log(porqeuria);
}

const cripty = async (data) => {
    let pass = await bcrypt.hash(data, 8);
    return pass;

}

const decripty = async (data, old) => {
    let aux = await bcrypt.compare(data, old);
    return aux;
}

const newUser = async (data) => {

    let aux = await cripty(data.password)
        .then(res => data.password = res)
        .catch(err => console.log(err));

    const tempUser = await new db.User({
        name: data.name,
        email: data.email,
        password: data.password
    }
    );
    const result = await tempUser.save();
    return result;
};


const findUserName = async (data) => {
    let aux2;
    let result = await db.User.find({ name: data.name })
        .then(resolve => {
            if (resolve.length == 0) {
                aux2 = false
            } else {
                aux2 = decripty(data.password, resolve[0].password)
                    .then();
            }
        });
    return aux2;
}


const findEmail = async (data) => {
    let aux2;
    let result = await db.User.find({ email: data.name })
        .then(resolve => {
            if (resolve.length == 0) {
                aux2 = false
            } else {
                aux2 = decripty(data.password, resolve[0].password)
                    .then();
            }
        });
    return aux2;
}

const findUser = async (data) => {
    if (data.name.includes("@") && data.name.includes(".com")) {
        let aux = await findEmail(data);
        return aux;
    } else {
        let aux = await findUserName(data);
        return aux;
    }
}

const newTokensing = (data) => {
    const token = jwt.sign(data.name, data.password);
    console.log(`Token ${data.name}: ${token}`);
    return token;
}

const consumeToken = (token, firm) => {
    const detoken = jwt.verify(token, firm);
    console.log(`Token ${detoken}`);
}

const searchEmail = async (email) => {
    let mjs = "";
    let result = await db.User.find({ email: email })
        .then(resolve => {
            (resolve.length == 0) ? (mjs = false) : (mjs = true)
        });
    return mjs;
}

const searchName = async (name) => {
    let mjs = "";
    let result = await db.User.find({ name: name })
        .then(resolve => {
            (resolve.length == 0) ? (mjs = false) : (mjs = true)
        });
    return mjs;
}


const createUser = async (data) => {
    let result;
    let validator = await searchEmail(data.email);
    let validator2 = await searchName(data.name);

    if (validator == false && validator2 == false) {
        result = await newUser(data);
        return `Usuario/a ${data.name} creado/a existosamente`;
    } else {


        (validator != true) ? (result = `El nombre de usuario ya esta en uso`) : ('');
        (validator2 != true) ? (result = `El email ya esta en uso`) : ('');
        (validator == true && validator2 == true) ? (result = `El email y el nombre de usuario ya estan en uso`) : ('');

        throw result;
    }
}




module.exports = {
    createUser,
    findUser,
    newTokensing,
    consumeToken
};