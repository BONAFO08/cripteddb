const mongoose = require('mongoose');
const mongoUrl = `mongodb://localhost:27017/baseEncriptada`;
const users = require('./mdlUsers');


const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}


mongoose.connect(mongoUrl, options);

const User = mongoose.model('users', users);


module.exports = {
    mongoose,
    User
};