const controllers = require('./controllers');

const verify = async (req, res, param) => {
    let hash;
    switch (param) {
        case "cr":
            hash = await controllers.createUser(req.body)
                .then(resolve => res.status(200).json(resolve))
                .catch(err => res.status(403).send(err));
            break;
        case "de":
            hash = await controllers.findUser(req.body);
            if (hash == true) {
                let token = controllers.newTokensing(req.body);
                controllers.consumeToken(token, req.body.password);
                res.status(200).json(`こんにちは ${req.body.name}`);
            } else {
                res.status(400).send('パスワードが正しくありません');
            }
            break;
    }

}

module.exports = {
    verify
}