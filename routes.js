const express = require("express");
const router = express.Router();
const middle = require("./middle");


/**
 * @swagger
 * /newUser:
 *  post:
 *    tags: 
 *      [Cards]
 *    summary: Crear carta
 *    description: Crea una nueva carta
 *    parameters:
 *    - name: name
 *      description: Nombre de la carta (Ingles)
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Nombre de la carta (Japones)
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Raza de la carta
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: Pedido exitoso
 *            404:
 *                description: Usuario o producto no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */
router.post('/newUser', (req, res) => {
    middle.verify(req, res, 'cr');
});

/**
 * @swagger
 * /logIn:
 *  post:
 *    tags: 
 *      [Cards]
 *    summary: Crear carta
 *    description: Crea una nueva carta
 *    parameters:
 *    - name: name
 *      description: Nombre de la carta (Ingles)
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Raza de la carta
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: Pedido exitoso
 *            404:
 *                description: Usuario o producto no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */
router.post('/logIn', (req, res) => {
    middle.verify(req, res, 'de');
});

module.exports = router;